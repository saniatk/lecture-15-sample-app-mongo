define([
    'backbone.marionette',
    './LayoutView'
], function (Marionette, LayoutView) {
    return Marionette.Application.extend({
        initialize: function () {
            this.layout = new LayoutView();
            this.layout.render();
        }
    });
});