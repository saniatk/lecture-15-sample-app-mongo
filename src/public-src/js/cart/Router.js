define([
    'backbone',
    '../cart/Service',
    './ItemView',
    './Model'
], function (Backbone, CartService, ItemView, Model) {
    return Backbone.Router.extend({

        initialize: function (options) {
            this.container = options.container;
        },

        routes: {
            cart: 'index'
        },

        index: function () {
            var model = new Model();
            model
                .fetch({
                    data: {
                        cart: CartService.serialize()
                    }
                })
                .done(function () {
                    var view = new ItemView({
                        model: model
                    });
                    this.container.show(view);
                }.bind(this));
        }
    });
});