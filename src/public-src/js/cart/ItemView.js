define([
    'backbone',
    'backbone.marionette',
    './template',
    './Service',
    './Model'
], function (Backbone, Marionette, template, CartService, Model) {
    return Marionette.ItemView.extend({
        template: template,

        ui: {
            input: 'input'
        },

        events: {
            'click button[data-action=recalculate]': 'recalculate',
            'click button[data-action=order]': 'order'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        _updateCart: function () {
            this.ui.input.each(function () {
                CartService.add(this.dataset.product, parseInt(this.value) || 0);
            });
        },

        recalculate: function () {
            this._updateCart();
            this.model.fetch({
                data: {
                    cart: CartService.serialize()
                }
            })
        },

        order: function () {
            var email = window.prompt('Provide us with your email, please!');
            if (email) {
                var phone = window.prompt('Provide us with your phone number, please!');
                if (phone) {
                    this._updateCart();
                    new Model({
                        email: email,
                        phone: phone,
                        items: CartService.serialize()
                    })
                        .save()
                        .done(function (data) {
                            CartService.removeAll();
                            Backbone.history.navigate('order/' + data.hash, {trigger: true, replace: true});
                        });
                }
            }
        }
    });
});