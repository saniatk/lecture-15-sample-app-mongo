define([
    'backbone.marionette',
    './template'
], function (Marionette, template) {
    return Marionette.ItemView.extend({
        tagName: 'a',
        attributes: {
            href: '#cart'
        },

        initialize: function () {
            this.listenTo(this.collection, 'change add remove', this.render);
        },

        render: function () {
            this.$el.html(template({count: this.collection.length}));
            return this;
        }
    });
});